﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CodeChecker.common
{
    static class Help
    {
        public static int GetPercentOfPartnershipEnergies(int result, int option)
        {
            int percent = 0;
            switch (option)
            {
                case 0:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_Rozwoju
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 1:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_Rozwoju_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
            }
            return percent;
        }

        public static int GetPercentOfRestEnergies(int result, int option)
        {
            int percent = 0;
            switch (option)
            {
                case 0:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).TrzyEnergie_ResztaEnergii
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 1:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).TrzyEnergie_ResztaEnergii_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 2:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_ResztaEnergii
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 3:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_ResztaEnergii_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
            }
            return percent;
        }

        public static int GetPercentOfEnergyWE(int result, int option)
        {
            int percent = 0;
            switch (option)
            {
                case 0:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).EnergieWE
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 1:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).TrzyEnergie_ResztaEnergii_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 2:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_ResztaEnergii
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 3:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_ResztaEnergii_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
            }
            return percent;
        }

        public static int GetPercentOfSexualEnergies(int result, int option)
        {
            int percent = 0;
            switch (option)
            {
                case 0:
                    percent = (
                                from se in (new ylcdatabaseDataContext()).TrzyEnergie_SeksualneEnergie_IN
                                where (double)result >= se.@from && (double)result <= se.to
                                select se.percent).FirstOrDefault<int>();
                    break;
                case 1:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).TrzyEnergie_SeksualneEnergie_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 2:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_SeksualneEnergie
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 3:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_SeksualneEnergie_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
            }
            return percent;
        }

        public static int GetPercentOfSpiritualEnergies(int result, int option)
        {
            int percent = 0;
            switch (option)
            {
                case 0:
                    percent = (from se in (new ylcdatabaseDataContext()).TrzyEnergie_DuchoweEnergie
                               where (double)result >= se.@from && (double)result <= se.to
                               select se.percent).FirstOrDefault<int>();
                    break;
                case 1:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).TrzyEnergie_DuchoweEnergie_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 2:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_DuchoweEnergie
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
                case 3:
                    percent = (
                                from re in (new ylcdatabaseDataContext()).CzteryEnergie_DuchoweEnergie_IN
                                where (double)result >= re.@from && (double)result <= re.to
                                select re.percent).FirstOrDefault<int>();
                    break;
            }

            return percent;
        }

        public static void SendMail(string email, string subject, string content, string attachment = null)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("info@yourlovekey.com");
                mailMessage.Subject = subject;
                mailMessage.Body = content;
                mailMessage.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(attachment))
                {
                    mailMessage.Attachments.Add(new Attachment(attachment));
                }
                mailMessage.To.Add(new MailAddress(email));
                SmtpClient smtp = new SmtpClient()
                {
                    Host = "smtp.ionos.de",
                    EnableSsl = true
                };
                NetworkCredential NetworkCred = new NetworkCredential()
                {
                    UserName = "info@yourlovekey.com",
                    Password = "Prodigy3439!"
                };
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mailMessage);
            }
        }

        public static double ObliczBiorytmIntelektualny2(string data, string checkDate)
        {
            int cykl = 33;
            double pi = Math.PI;
            DateTime d1 = DateTime.Parse(data);
            DateTime d2 = DateTime.Parse(checkDate);
            TimeSpan t = d2.Subtract(d1);

            return Math.Round(Math.Sin(2 * pi * (t.TotalDays) / cykl), 2);
        }

        public static double ObliczBiorytmEmocjonalny2(string data, string checkDate)
        {
            int cykl = 28;
            double pi = Math.PI;
            DateTime d1 = DateTime.Parse(data);
            DateTime d2 = DateTime.Parse(checkDate);
            TimeSpan t = d2.Subtract(d1);

            return Math.Round(Math.Sin(2 * pi * (t.TotalDays) / cykl), 2);
        }

        public static double ObliczBiorytmFizyczny2(string data, string checkDate)
        {
            int cykl = 23;
            double pi = Math.PI;
            DateTime d1 = DateTime.Parse(data);
            DateTime d2 = DateTime.Parse(checkDate);
            TimeSpan t = d2.Subtract(d1);

            return Math.Round(Math.Sin(2 * pi * (t.TotalDays) / cykl), 2);
        }

        public static double ObliczBiorytmDuchowy(string data, string checkDate)
        {
            int cykl = 53;
            double pi = Math.PI;
            DateTime d1 = DateTime.Parse(data);
            DateTime d2 = DateTime.Parse(checkDate);
            TimeSpan t = d2.Subtract(d1);

            return Math.Round(Math.Sin(2 * pi * (t.TotalDays) / cykl), 2);
        }

        public static double ObliczBiorytmIntuicyjny(string data, string checkDate)
        {
            int cykl = 38;
            double pi = Math.PI;
            DateTime d1 = DateTime.Parse(data);
            DateTime d2 = DateTime.Parse(checkDate);
            TimeSpan t = d2.Subtract(d1);

            return Math.Round(Math.Sin(2 * pi * (t.TotalDays) / cykl), 2);
        }

        public static double ObliczBiorytmEstetyczny(string data, string checkDate)
        {
            int cykl = 43;
            double pi = Math.PI;
            DateTime d1 = DateTime.Parse(data);
            DateTime d2 = DateTime.Parse(checkDate);
            TimeSpan t = d2.Subtract(d1);

            return Math.Round(Math.Sin(2 * pi * (t.TotalDays) / cykl), 2);
        }

        public static double ObliczBiorytmSwiadomosci(string data, string checkDate)
        {
            int cykl = 48;
            double pi = Math.PI;
            DateTime d1 = DateTime.Parse(data);
            DateTime d2 = DateTime.Parse(checkDate);
            TimeSpan t = d2.Subtract(d1);

            return Math.Round(Math.Sin(2 * pi * (t.TotalDays) / cykl), 2);
        }

        public static double CeilingOrFloor(double w)
        {
            double w_int = Math.Floor(w);

            if (w - w_int >= 0.5)
            {
                return Math.Ceiling(w);
            }
            else
            {
                return Math.Floor(w);
            }

        }

        internal static int? GetGwiazdki(int sumaData)
        {
            if (sumaData >= 5000 && sumaData < 6000)
            {
                return 1;
            }
            else if (sumaData >= 6000 && sumaData < 7000)
            {
                return 2;
            }
            else if (sumaData >= 7000 && sumaData < 8000)
            {
                return 3;
            }
            else if (sumaData >= 8000 && sumaData < 9000)
            {
                return 4;
            }
            else if (sumaData >= 9000 && sumaData < 10000)
            {
                return 5;
            }
            else if (sumaData >= 10000 && sumaData < 11000)
            {
                return 6;
            }
            else if (sumaData >= 11000 && sumaData < 12000)
            {
                return 7;
            }
            else if (sumaData >= 12000 && sumaData < 13000)
            {
                return 8;
            }
            else if (sumaData >= 13000 && sumaData < 14000)
            {
                return 9;
            }
            else if (sumaData >= 14000)
            {
                return 10;
            }
            else
            {
                return 0;
            }
        }
    }
}
