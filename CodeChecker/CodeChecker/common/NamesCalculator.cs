﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChecker.common
{
	public static class NamesCalculator
	{
		public static string Imie(string a, string b, char sex1, char sex2, string data1, string data2, ref int rest_energies, ref int spiritual_energies, ref int sexual_energies, ref int partnership_energies, bool success_forecast)
		{
			decimal num;
			int num1;

			a = a.ToLower();
			b = b.ToLower();
			a = a.Replace(" ", "");
			b = b.Replace(" ", "");
			a = a.Replace("ä", "ae");
			a = a.Replace("ö", "oe");
			a = a.Replace("ü", "ue");
			a = a.Replace("ß", "ss");
			b = b.Replace("ä", "ae");
			b = b.Replace("ö", "oe");
			b = b.Replace("ü", "ue");
			b = b.Replace("ß", "ss");
			string wynik = "";
			ylcdatabaseDataContext db = new ylcdatabaseDataContext();
			generowanieWynikuINexcelWebResult wyniki = null;
			generowanieWynikuINexcelWebResult wyniki2 = null;
			if (sex1 == 'F' && sex2 == 'F')
			{
				string _tmp = "";
				string _tmp1 = "";
				char? nullable69 = new char?(sex1);
				char? nullable70 = new char?(sex2);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie = db.generowanieWynikuINexcelWeb(nullable69, nullable70, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				_tmp = string.Concat(_tmp, imie.suma, ";");
				_tmp = string.Concat(_tmp, imie.k24_1, ";");
				_tmp = string.Concat(_tmp, imie.k25_1, ";");
				_tmp = string.Concat(_tmp, imie.k31, ";");
				_tmp = string.Concat(_tmp, imie.k32, ";");
				_tmp = string.Concat(_tmp, imie.k33, ";");
				_tmp = string.Concat(_tmp, imie.k39, ";");
				_tmp = string.Concat(_tmp, imie.k42, ";");
				_tmp = string.Concat(_tmp, imie.k43, ";");
				_tmp = string.Concat(_tmp, imie.k83_a, ";");
				_tmp = string.Concat(_tmp, imie.k83_b, ";");
				_tmp = string.Concat(_tmp, imie.k83_c, ";");
				_tmp = string.Concat(_tmp, imie.k83_d, ";");
				_tmp = string.Concat(_tmp, imie.k83_e, ";");
				_tmp = string.Concat(_tmp, imie.k83_f, ";");
				_tmp = string.Concat(_tmp, imie.k88, ";");
				_tmp = string.Concat(_tmp, imie.k120, ";");
				_tmp = string.Concat(_tmp, imie.k121, ";");
				_tmp = string.Concat(_tmp, imie.k122, ";");
				_tmp = string.Concat(_tmp, imie.k123, ";");
				_tmp = string.Concat(_tmp, imie.k124, ";");
				_tmp = string.Concat(_tmp, imie.k125, ";");
				_tmp = string.Concat(_tmp, imie.k126, ";");
				_tmp = string.Concat(_tmp, imie.k127);
				wyniki = (wyniki == null ? imie : wyniki);
				char? nullable71 = new char?(sex2);
				char? nullable72 = new char?(sex1);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie1 = db.generowanieWynikuINexcelWeb(nullable71, nullable72, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				_tmp1 = string.Concat(_tmp1, imie1.suma, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k24_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k25_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k31, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k32, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k33, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k39, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k42, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k43, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_a, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_b, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_c, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_d, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_e, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_f, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k88, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k120, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k121, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k122, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k123, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k124, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k125, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k126, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k127);
				wyniki2 = (wyniki2 == null ? imie1 : wyniki2);
				string[] tmp = _tmp.Split(new char[] { ';' });
				string[] tmp1 = _tmp1.Split(new char[] { ';' });
				num1 = (Convert.ToInt32(tmp[0]) + Convert.ToInt32(tmp1[0])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[1]) + Convert.ToInt32(tmp1[1])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[2]) + Convert.ToInt32(tmp1[2])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[3]) + Convert.ToInt32(tmp1[3])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[4]) + Convert.ToInt32(tmp1[4])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[5]) + Convert.ToInt32(tmp1[5])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[6]) + Convert.ToInt32(tmp1[6])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[7]) + Convert.ToInt32(tmp1[7])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[8]) + Convert.ToInt32(tmp1[8])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[9]) + Convert.ToInt32(tmp1[9])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[10]) + Convert.ToInt32(tmp1[10])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[11]) + Convert.ToInt32(tmp1[11])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[12]) + Convert.ToInt32(tmp1[12])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[13]) + Convert.ToInt32(tmp1[13])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[14]) + Convert.ToInt32(tmp1[14])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[15]) + Convert.ToInt32(tmp1[15])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[16]) + Convert.ToInt32(tmp1[16])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[17]) + Convert.ToInt32(tmp1[17])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[18]) + Convert.ToInt32(tmp1[18])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[19]) + Convert.ToInt32(tmp1[19])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[20]) + Convert.ToInt32(tmp1[20])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[21]) + Convert.ToInt32(tmp1[21])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[22]) + Convert.ToInt32(tmp1[22])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[23]) + Convert.ToInt32(tmp1[23])) / 2;
				wynik = string.Concat(wynik, num1.ToString());
			}
			else if (sex1 == 'M' && sex2 == 'M')
			{
				string _tmp = "";
				string _tmp1 = "";
				char? nullable73 = new char?(sex1);
				char? nullable74 = new char?(sex2);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie = db.generowanieWynikuINexcelWeb(nullable73, nullable74, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				_tmp = string.Concat(_tmp, imie.suma, ";");
				_tmp = string.Concat(_tmp, imie.k24_1, ";");
				_tmp = string.Concat(_tmp, imie.k25_1, ";");
				_tmp = string.Concat(_tmp, imie.k31, ";");
				_tmp = string.Concat(_tmp, imie.k32, ";");
				_tmp = string.Concat(_tmp, imie.k33, ";");
				_tmp = string.Concat(_tmp, imie.k39, ";");
				_tmp = string.Concat(_tmp, imie.k42, ";");
				_tmp = string.Concat(_tmp, imie.k43, ";");
				_tmp = string.Concat(_tmp, imie.k83_a, ";");
				_tmp = string.Concat(_tmp, imie.k83_b, ";");
				_tmp = string.Concat(_tmp, imie.k83_c, ";");
				_tmp = string.Concat(_tmp, imie.k83_d, ";");
				_tmp = string.Concat(_tmp, imie.k83_e, ";");
				_tmp = string.Concat(_tmp, imie.k83_f, ";");
				_tmp = string.Concat(_tmp, imie.k88, ";");
				_tmp = string.Concat(_tmp, imie.k120, ";");
				_tmp = string.Concat(_tmp, imie.k121, ";");
				_tmp = string.Concat(_tmp, imie.k122, ";");
				_tmp = string.Concat(_tmp, imie.k123, ";");
				_tmp = string.Concat(_tmp, imie.k124, ";");
				_tmp = string.Concat(_tmp, imie.k125, ";");
				_tmp = string.Concat(_tmp, imie.k126, ";");
				_tmp = string.Concat(_tmp, imie.k127);
				wyniki = (wyniki == null ? imie : wyniki);
				char? nullable75 = new char?(sex2);
				char? nullable76 = new char?(sex1);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie1 = db.generowanieWynikuINexcelWeb(nullable75, nullable76, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				_tmp1 = string.Concat(_tmp1, imie1.suma, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k24_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k25_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k31, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k32, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k33, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k39, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k42, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k43, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_a, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_b, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_c, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_d, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_e, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_f, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k88, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k120, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k121, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k122, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k123, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k124, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k125, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k126, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k127);
				wyniki2 = (wyniki2 == null ? imie1 : wyniki2);
				string[] tmp = _tmp.Split(new char[] { ';' });
				string[] tmp1 = _tmp1.Split(new char[] { ';' });
				num1 = (Convert.ToInt32(tmp[0]) + Convert.ToInt32(tmp1[0])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[1]) + Convert.ToInt32(tmp1[1])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[2]) + Convert.ToInt32(tmp1[2])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[3]) + Convert.ToInt32(tmp1[3])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[4]) + Convert.ToInt32(tmp1[4])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[5]) + Convert.ToInt32(tmp1[5])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[6]) + Convert.ToInt32(tmp1[6])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[7]) + Convert.ToInt32(tmp1[7])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[8]) + Convert.ToInt32(tmp1[8])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[9]) + Convert.ToInt32(tmp1[9])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[10]) + Convert.ToInt32(tmp1[10])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[11]) + Convert.ToInt32(tmp1[11])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[12]) + Convert.ToInt32(tmp1[12])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[13]) + Convert.ToInt32(tmp1[13])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[14]) + Convert.ToInt32(tmp1[14])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[15]) + Convert.ToInt32(tmp1[15])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[16]) + Convert.ToInt32(tmp1[16])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[17]) + Convert.ToInt32(tmp1[17])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[18]) + Convert.ToInt32(tmp1[18])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[19]) + Convert.ToInt32(tmp1[19])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[20]) + Convert.ToInt32(tmp1[20])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[21]) + Convert.ToInt32(tmp1[21])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[22]) + Convert.ToInt32(tmp1[22])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[23]) + Convert.ToInt32(tmp1[23])) / 2;
				wynik = string.Concat(wynik, num1.ToString());
			}
			else if (sex1 != 'M')
			{
				char? nullable77 = new char?(sex2);
				char? nullable78 = new char?(sex1);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie = db.generowanieWynikuINexcelWeb(nullable77, nullable78, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				wynik = string.Concat(wynik, imie.suma, ";");
				wynik = string.Concat(wynik, imie.k24_1, ";");
				wynik = string.Concat(wynik, imie.k25_1, ";");
				wynik = string.Concat(wynik, imie.k31, ";");
				wynik = string.Concat(wynik, imie.k32, ";");
				wynik = string.Concat(wynik, imie.k33, ";");
				wynik = string.Concat(wynik, imie.k39, ";");
				wynik = string.Concat(wynik, imie.k42, ";");
				wynik = string.Concat(wynik, imie.k43, ";");
				wynik = string.Concat(wynik, imie.k83_a, ";");
				wynik = string.Concat(wynik, imie.k83_b, ";");
				wynik = string.Concat(wynik, imie.k83_c, ";");
				wynik = string.Concat(wynik, imie.k83_d, ";");
				wynik = string.Concat(wynik, imie.k83_e, ";");
				wynik = string.Concat(wynik, imie.k83_f, ";");
				wynik = string.Concat(wynik, imie.k88, ";");
				wynik = string.Concat(wynik, imie.k120, ";");
				wynik = string.Concat(wynik, imie.k121, ";");
				wynik = string.Concat(wynik, imie.k122, ";");
				wynik = string.Concat(wynik, imie.k123, ";");
				wynik = string.Concat(wynik, imie.k124, ";");
				wynik = string.Concat(wynik, imie.k125, ";");
				wynik = string.Concat(wynik, imie.k126, ";");
				wynik = string.Concat(wynik, imie.k127);
				wyniki = (wyniki == null ? imie : wyniki);
			}
			else
			{
				char? nullable79 = new char?(sex1);
				char? nullable80 = new char?(sex2);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie = db.generowanieWynikuINexcelWeb(nullable79, nullable80, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				wynik = string.Concat(wynik, imie.suma, ";");
				wynik = string.Concat(wynik, imie.k24_1, ";");
				wynik = string.Concat(wynik, imie.k25_1, ";");
				wynik = string.Concat(wynik, imie.k31, ";");
				wynik = string.Concat(wynik, imie.k32, ";");
				wynik = string.Concat(wynik, imie.k33, ";");
				wynik = string.Concat(wynik, imie.k39, ";");
				wynik = string.Concat(wynik, imie.k42, ";");
				wynik = string.Concat(wynik, imie.k43, ";");
				wynik = string.Concat(wynik, imie.k83_a, ";");
				wynik = string.Concat(wynik, imie.k83_b, ";");
				wynik = string.Concat(wynik, imie.k83_c, ";");
				wynik = string.Concat(wynik, imie.k83_d, ";");
				wynik = string.Concat(wynik, imie.k83_e, ";");
				wynik = string.Concat(wynik, imie.k83_f, ";");
				wynik = string.Concat(wynik, imie.k88, ";");
				wynik = string.Concat(wynik, imie.k120, ";");
				wynik = string.Concat(wynik, imie.k121, ";");
				wynik = string.Concat(wynik, imie.k122, ";");
				wynik = string.Concat(wynik, imie.k123, ";");
				wynik = string.Concat(wynik, imie.k124, ";");
				wynik = string.Concat(wynik, imie.k125, ";");
				wynik = string.Concat(wynik, imie.k126, ";");
				wynik = string.Concat(wynik, imie.k127);
				wyniki = (wyniki == null ? imie : wyniki);
			}
			if (wyniki2 != null)
			{
				rest_energies += Convert.ToInt32((wyniki.W25_1 + wyniki2.W25_1) / 2);
				rest_energies += Convert.ToInt32((wyniki.W31 + wyniki2.W31) / 2);
				rest_energies += Convert.ToInt32((wyniki.W32 + wyniki2.W32) / 2);
				rest_energies += Convert.ToInt32((wyniki.W33 + wyniki2.W33) / 2);
				rest_energies += Convert.ToInt32((wyniki.W39 + wyniki2.W39) / 2);
				rest_energies += Convert.ToInt32((wyniki.W42 + wyniki2.W42) / 2);
				rest_energies += Convert.ToInt32((wyniki.W43 + wyniki2.W43) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_a + wyniki2.W83_a) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_b + wyniki2.W83_b) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_c + wyniki2.W83_c) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_d + wyniki2.W83_d) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_e + wyniki2.W83_e) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_f + wyniki2.W83_f) / 2);
				rest_energies += Convert.ToInt32((wyniki.W88 + wyniki2.W88) / 2);

				spiritual_energies += Convert.ToInt32((wyniki.W24_1 + wyniki2.W24_1) / 2);

				if (success_forecast)
				{
					partnership_energies += Convert.ToInt32((wyniki.W120 + wyniki2.W120) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W121 + wyniki2.W121) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W122 + wyniki2.W122) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W123 + wyniki2.W123) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W124 + wyniki2.W124) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W125 + wyniki2.W125) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W126 + wyniki2.W126) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W127 + wyniki2.W127) / 2);
				}
				else
				{
					rest_energies += Convert.ToInt32((wyniki.W120 + wyniki2.W120) / 2);
					rest_energies += Convert.ToInt32((wyniki.W121 + wyniki2.W121) / 2);
					rest_energies += Convert.ToInt32((wyniki.W122 + wyniki2.W122) / 2);
					rest_energies += Convert.ToInt32((wyniki.W123 + wyniki2.W123) / 2);
					rest_energies += Convert.ToInt32((wyniki.W124 + wyniki2.W124) / 2);
					rest_energies += Convert.ToInt32((wyniki.W125 + wyniki2.W125) / 2);
					rest_energies += Convert.ToInt32((wyniki.W126 + wyniki2.W126) / 2);
					rest_energies += Convert.ToInt32((wyniki.W127 + wyniki2.W127) / 2);
				}

			}
			else
			{
				rest_energies += Convert.ToInt32(wyniki.W25_1)
				+ Convert.ToInt32(wyniki.W31)
				+ Convert.ToInt32(wyniki.W32)
				+ Convert.ToInt32(wyniki.W33)
				+ Convert.ToInt32(wyniki.W39)
				+ Convert.ToInt32(wyniki.W42)
				+ Convert.ToInt32(wyniki.W43)
				+ Convert.ToInt32(wyniki.W83_a)
				+ Convert.ToInt32(wyniki.W83_b)
				+ Convert.ToInt32(wyniki.W83_c)
				+ Convert.ToInt32(wyniki.W83_d)
				+ Convert.ToInt32(wyniki.W83_e)
				+ Convert.ToInt32(wyniki.W83_f)
				+ Convert.ToInt32(wyniki.W88);

				spiritual_energies += Convert.ToInt32(wyniki.W24_1);

				if (success_forecast)
				{
					partnership_energies += Convert.ToInt32(wyniki.W120)
						+ Convert.ToInt32(wyniki.W121)
						+ Convert.ToInt32(wyniki.W122)
						+ Convert.ToInt32(wyniki.W123)
						+ Convert.ToInt32(wyniki.W124)
						+ Convert.ToInt32(wyniki.W125)
						+ Convert.ToInt32(wyniki.W126)
						+ Convert.ToInt32(wyniki.W127);
				}
				{
					rest_energies += Convert.ToInt32(wyniki.W120)
						+ Convert.ToInt32(wyniki.W121)
						+ Convert.ToInt32(wyniki.W122)
						+ Convert.ToInt32(wyniki.W123)
						+ Convert.ToInt32(wyniki.W124)
						+ Convert.ToInt32(wyniki.W125)
						+ Convert.ToInt32(wyniki.W126)
						+ Convert.ToInt32(wyniki.W127);
				}
			}
			return wynik;
		}

		public static string Imie_w(string a, string b, char sex1, char sex2, string data1, string data2, ref int rest_energies, ref int spiritual_energies, ref int sexual_energies, ref int partnership_energies, bool success_forecast)
		{
			if(a == null)
            {
				a = "";
            }

			if (b == null)
			{
				b = "";
			}

			decimal num;
			int num1;

			a = a.ToLower();
			b = b.ToLower();
			a = a.Replace(" ", "");
			b = b.Replace(" ", "");
			a = a.Replace("ä", "ae");
			a = a.Replace("ö", "oe");
			a = a.Replace("ü", "ue");
			a = a.Replace("ß", "ss");
			b = b.Replace("ä", "ae");
			b = b.Replace("ö", "oe");
			b = b.Replace("ü", "ue");
			b = b.Replace("ß", "ss");
			string wynik = "";
			ylcdatabaseDataContext db = new ylcdatabaseDataContext();
			generowanieWynikuINexcelWebResult wyniki = null;
			generowanieWynikuINexcelWebResult wyniki2 = null;
			if (sex1 == 'F' && sex2 == 'F')
			{
				string _tmp = "";
				string _tmp1 = "";
				char? nullable69 = new char?(sex1);
				char? nullable70 = new char?(sex2);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie = db.generowanieWynikuINexcelWeb(nullable69, nullable70, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				_tmp = string.Concat(_tmp, imie.suma, ";");
				_tmp = string.Concat(_tmp, imie.W24_1, ";");
				_tmp = string.Concat(_tmp, imie.W25_1, ";");
				_tmp = string.Concat(_tmp, imie.W31, ";");
				_tmp = string.Concat(_tmp, imie.W32, ";");
				_tmp = string.Concat(_tmp, imie.W33, ";");
				_tmp = string.Concat(_tmp, imie.W39, ";");
				_tmp = string.Concat(_tmp, imie.W42, ";");
				_tmp = string.Concat(_tmp, imie.W43, ";");
				_tmp = string.Concat(_tmp, imie.W83_a, ";");
				_tmp = string.Concat(_tmp, imie.W83_b, ";");
				_tmp = string.Concat(_tmp, imie.W83_c, ";");
				_tmp = string.Concat(_tmp, imie.W83_d, ";");
				_tmp = string.Concat(_tmp, imie.W83_e, ";");
				_tmp = string.Concat(_tmp, imie.W83_f, ";");
				_tmp = string.Concat(_tmp, imie.W88, ";");
				_tmp = string.Concat(_tmp, imie.W120, ";");
				_tmp = string.Concat(_tmp, imie.W121, ";");
				_tmp = string.Concat(_tmp, imie.W122, ";");
				_tmp = string.Concat(_tmp, imie.W123, ";");
				_tmp = string.Concat(_tmp, imie.W124, ";");
				_tmp = string.Concat(_tmp, imie.W125, ";");
				_tmp = string.Concat(_tmp, imie.W126, ";");
				_tmp = string.Concat(_tmp, imie.W127);
				wyniki = (wyniki == null ? imie : wyniki);
				char? nullable71 = new char?(sex2);
				char? nullable72 = new char?(sex1);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie1 = db.generowanieWynikuINexcelWeb(nullable71, nullable72, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				_tmp1 = string.Concat(_tmp1, imie1.suma, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W24_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W25_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W31, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W32, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W33, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W39, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W42, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W43, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_a, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_b, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_c, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_d, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_e, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_f, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W88, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W120, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W121, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W122, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W123, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W124, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W125, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W126, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W127);
				wyniki2 = (wyniki2 == null ? imie1 : wyniki2);
				string[] tmp = _tmp.Split(new char[] { ';' });
				string[] tmp1 = _tmp1.Split(new char[] { ';' });
				num1 = (Convert.ToInt32(tmp[0]) + Convert.ToInt32(tmp1[0])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[1]) + Convert.ToInt32(tmp1[1])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[2]) + Convert.ToInt32(tmp1[2])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[3]) + Convert.ToInt32(tmp1[3])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[4]) + Convert.ToInt32(tmp1[4])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[5]) + Convert.ToInt32(tmp1[5])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[6]) + Convert.ToInt32(tmp1[6])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[7]) + Convert.ToInt32(tmp1[7])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[8]) + Convert.ToInt32(tmp1[8])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[9]) + Convert.ToInt32(tmp1[9])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[10]) + Convert.ToInt32(tmp1[10])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[11]) + Convert.ToInt32(tmp1[11])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[12]) + Convert.ToInt32(tmp1[12])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[13]) + Convert.ToInt32(tmp1[13])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[14]) + Convert.ToInt32(tmp1[14])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[15]) + Convert.ToInt32(tmp1[15])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[16]) + Convert.ToInt32(tmp1[16])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[17]) + Convert.ToInt32(tmp1[17])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[18]) + Convert.ToInt32(tmp1[18])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[19]) + Convert.ToInt32(tmp1[19])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[20]) + Convert.ToInt32(tmp1[20])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[21]) + Convert.ToInt32(tmp1[21])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[22]) + Convert.ToInt32(tmp1[22])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[23]) + Convert.ToInt32(tmp1[23])) / 2;
				wynik = string.Concat(wynik, num1.ToString());
			}
			else if (sex1 == 'M' && sex2 == 'M')
			{
				string _tmp = "";
				string _tmp1 = "";
				char? nullable73 = new char?(sex1);
				char? nullable74 = new char?(sex2);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie = db.generowanieWynikuINexcelWeb(nullable73, nullable74, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				_tmp = string.Concat(_tmp, imie.suma, ";");
				_tmp = string.Concat(_tmp, imie.W24_1, ";");
				_tmp = string.Concat(_tmp, imie.W25_1, ";");
				_tmp = string.Concat(_tmp, imie.W31, ";");
				_tmp = string.Concat(_tmp, imie.W32, ";");
				_tmp = string.Concat(_tmp, imie.W33, ";");
				_tmp = string.Concat(_tmp, imie.W39, ";");
				_tmp = string.Concat(_tmp, imie.W42, ";");
				_tmp = string.Concat(_tmp, imie.W43, ";");
				_tmp = string.Concat(_tmp, imie.W83_a, ";");
				_tmp = string.Concat(_tmp, imie.W83_b, ";");
				_tmp = string.Concat(_tmp, imie.W83_c, ";");
				_tmp = string.Concat(_tmp, imie.W83_d, ";");
				_tmp = string.Concat(_tmp, imie.W83_e, ";");
				_tmp = string.Concat(_tmp, imie.W83_f, ";");
				_tmp = string.Concat(_tmp, imie.W88, ";");
				_tmp = string.Concat(_tmp, imie.W120, ";");
				_tmp = string.Concat(_tmp, imie.W121, ";");
				_tmp = string.Concat(_tmp, imie.W122, ";");
				_tmp = string.Concat(_tmp, imie.W123, ";");
				_tmp = string.Concat(_tmp, imie.W124, ";");
				_tmp = string.Concat(_tmp, imie.W125, ";");
				_tmp = string.Concat(_tmp, imie.W126, ";");
				_tmp = string.Concat(_tmp, imie.W127);
				wyniki = (wyniki == null ? imie : wyniki);
				char? nullable75 = new char?(sex2);
				char? nullable76 = new char?(sex1);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie1 = db.generowanieWynikuINexcelWeb(nullable75, nullable76, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				_tmp1 = string.Concat(_tmp1, imie1.suma, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W24_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W25_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W31, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W32, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W33, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W39, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W42, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W43, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_a, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_b, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_c, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_d, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_e, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W83_f, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W88, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W120, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W121, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W122, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W123, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W124, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W125, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W126, ";");
				_tmp1 = string.Concat(_tmp1, imie1.W127);
				wyniki2 = (wyniki2 == null ? imie1 : wyniki2);
				string[] tmp = _tmp.Split(new char[] { ';' });
				string[] tmp1 = _tmp1.Split(new char[] { ';' });
				num1 = (Convert.ToInt32(tmp[0]) + Convert.ToInt32(tmp1[0])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[1]) + Convert.ToInt32(tmp1[1])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[2]) + Convert.ToInt32(tmp1[2])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[3]) + Convert.ToInt32(tmp1[3])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[4]) + Convert.ToInt32(tmp1[4])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[5]) + Convert.ToInt32(tmp1[5])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[6]) + Convert.ToInt32(tmp1[6])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[7]) + Convert.ToInt32(tmp1[7])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[8]) + Convert.ToInt32(tmp1[8])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[9]) + Convert.ToInt32(tmp1[9])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[10]) + Convert.ToInt32(tmp1[10])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[11]) + Convert.ToInt32(tmp1[11])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[12]) + Convert.ToInt32(tmp1[12])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[13]) + Convert.ToInt32(tmp1[13])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[14]) + Convert.ToInt32(tmp1[14])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[15]) + Convert.ToInt32(tmp1[15])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[16]) + Convert.ToInt32(tmp1[16])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[17]) + Convert.ToInt32(tmp1[17])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[18]) + Convert.ToInt32(tmp1[18])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[19]) + Convert.ToInt32(tmp1[19])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[20]) + Convert.ToInt32(tmp1[20])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[21]) + Convert.ToInt32(tmp1[21])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[22]) + Convert.ToInt32(tmp1[22])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[23]) + Convert.ToInt32(tmp1[23])) / 2;
				wynik = string.Concat(wynik, num1.ToString());
			}
			else if (sex1 != 'M')
			{
				char? nullable77 = new char?(sex2);
				char? nullable78 = new char?(sex1);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie = db.generowanieWynikuINexcelWeb(nullable77, nullable78, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				wynik = string.Concat(wynik, imie.suma, ";");
				wynik = string.Concat(wynik, imie.W24_1, ";");
				wynik = string.Concat(wynik, imie.W25_1, ";");
				wynik = string.Concat(wynik, imie.W31, ";");
				wynik = string.Concat(wynik, imie.W32, ";");
				wynik = string.Concat(wynik, imie.W33, ";");
				wynik = string.Concat(wynik, imie.W39, ";");
				wynik = string.Concat(wynik, imie.W42, ";");
				wynik = string.Concat(wynik, imie.W43, ";");
				wynik = string.Concat(wynik, imie.W83_a, ";");
				wynik = string.Concat(wynik, imie.W83_b, ";");
				wynik = string.Concat(wynik, imie.W83_c, ";");
				wynik = string.Concat(wynik, imie.W83_d, ";");
				wynik = string.Concat(wynik, imie.W83_e, ";");
				wynik = string.Concat(wynik, imie.W83_f, ";");
				wynik = string.Concat(wynik, imie.W88, ";");
				wynik = string.Concat(wynik, imie.W120, ";");
				wynik = string.Concat(wynik, imie.W121, ";");
				wynik = string.Concat(wynik, imie.W122, ";");
				wynik = string.Concat(wynik, imie.W123, ";");
				wynik = string.Concat(wynik, imie.W124, ";");
				wynik = string.Concat(wynik, imie.W125, ";");
				wynik = string.Concat(wynik, imie.W126, ";");
				wynik = string.Concat(wynik, imie.W127);
				wyniki = (wyniki == null ? imie : wyniki);
			}
			else
			{
				char? nullable79 = new char?(sex1);
				char? nullable80 = new char?(sex2);
				num = new decimal();
				generowanieWynikuINexcelWebResult imie = db.generowanieWynikuINexcelWeb(nullable79, nullable80, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebResult>();
				wynik = string.Concat(wynik, imie.suma, ";");
				wynik = string.Concat(wynik, imie.W24_1, ";");
				wynik = string.Concat(wynik, imie.W25_1, ";");
				wynik = string.Concat(wynik, imie.W31, ";");
				wynik = string.Concat(wynik, imie.W32, ";");
				wynik = string.Concat(wynik, imie.W33, ";");
				wynik = string.Concat(wynik, imie.W39, ";");
				wynik = string.Concat(wynik, imie.W42, ";");
				wynik = string.Concat(wynik, imie.W43, ";");
				wynik = string.Concat(wynik, imie.W83_a, ";");
				wynik = string.Concat(wynik, imie.W83_b, ";");
				wynik = string.Concat(wynik, imie.W83_c, ";");
				wynik = string.Concat(wynik, imie.W83_d, ";");
				wynik = string.Concat(wynik, imie.W83_e, ";");
				wynik = string.Concat(wynik, imie.W83_f, ";");
				wynik = string.Concat(wynik, imie.W88, ";");
				wynik = string.Concat(wynik, imie.W120, ";");
				wynik = string.Concat(wynik, imie.W121, ";");
				wynik = string.Concat(wynik, imie.W122, ";");
				wynik = string.Concat(wynik, imie.W123, ";");
				wynik = string.Concat(wynik, imie.W124, ";");
				wynik = string.Concat(wynik, imie.W125, ";");
				wynik = string.Concat(wynik, imie.W126, ";");
				wynik = string.Concat(wynik, imie.W127);
				wyniki = (wyniki == null ? imie : wyniki);
			}
			if (wyniki2 != null)
			{
				rest_energies += Convert.ToInt32((wyniki.W25_1 + wyniki2.W25_1) / 2);
				rest_energies += Convert.ToInt32((wyniki.W31 + wyniki2.W31) / 2);
				rest_energies += Convert.ToInt32((wyniki.W32 + wyniki2.W32) / 2);
				rest_energies += Convert.ToInt32((wyniki.W33 + wyniki2.W33) / 2);
				rest_energies += Convert.ToInt32((wyniki.W39 + wyniki2.W39) / 2);
				rest_energies += Convert.ToInt32((wyniki.W42 + wyniki2.W42) / 2);
				rest_energies += Convert.ToInt32((wyniki.W43 + wyniki2.W43) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_a + wyniki2.W83_a) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_b + wyniki2.W83_b) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_c + wyniki2.W83_c) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_d + wyniki2.W83_d) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_e + wyniki2.W83_e) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_f + wyniki2.W83_f) / 2);
				rest_energies += Convert.ToInt32((wyniki.W88 + wyniki2.W88) / 2);

				spiritual_energies += Convert.ToInt32((wyniki.W24_1 + wyniki2.W24_1) / 2);

				if (success_forecast)
				{
					partnership_energies += Convert.ToInt32((wyniki.W120 + wyniki2.W120) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W121 + wyniki2.W121) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W122 + wyniki2.W122) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W123 + wyniki2.W123) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W124 + wyniki2.W124) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W125 + wyniki2.W125) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W126 + wyniki2.W126) / 2);
					partnership_energies += Convert.ToInt32((wyniki.W127 + wyniki2.W127) / 2);
				}
				else
				{
					rest_energies += Convert.ToInt32((wyniki.W120 + wyniki2.W120) / 2);
					rest_energies += Convert.ToInt32((wyniki.W121 + wyniki2.W121) / 2);
					rest_energies += Convert.ToInt32((wyniki.W122 + wyniki2.W122) / 2);
					rest_energies += Convert.ToInt32((wyniki.W123 + wyniki2.W123) / 2);
					rest_energies += Convert.ToInt32((wyniki.W124 + wyniki2.W124) / 2);
					rest_energies += Convert.ToInt32((wyniki.W125 + wyniki2.W125) / 2);
					rest_energies += Convert.ToInt32((wyniki.W126 + wyniki2.W126) / 2);
					rest_energies += Convert.ToInt32((wyniki.W127 + wyniki2.W127) / 2);
				}

			}
			else
			{
				rest_energies += Convert.ToInt32(wyniki.W25_1)
				+ Convert.ToInt32(wyniki.W31)
				+ Convert.ToInt32(wyniki.W32)
				+ Convert.ToInt32(wyniki.W33)
				+ Convert.ToInt32(wyniki.W39)
				+ Convert.ToInt32(wyniki.W42)
				+ Convert.ToInt32(wyniki.W43)
				+ Convert.ToInt32(wyniki.W83_a)
				+ Convert.ToInt32(wyniki.W83_b)
				+ Convert.ToInt32(wyniki.W83_c)
				+ Convert.ToInt32(wyniki.W83_d)
				+ Convert.ToInt32(wyniki.W83_e)
				+ Convert.ToInt32(wyniki.W83_f)
				+ Convert.ToInt32(wyniki.W88);

				spiritual_energies += Convert.ToInt32(wyniki.W24_1);

				if (success_forecast)
				{
					partnership_energies += Convert.ToInt32(wyniki.W120)
						+ Convert.ToInt32(wyniki.W121)
						+ Convert.ToInt32(wyniki.W122)
						+ Convert.ToInt32(wyniki.W123)
						+ Convert.ToInt32(wyniki.W124)
						+ Convert.ToInt32(wyniki.W125)
						+ Convert.ToInt32(wyniki.W126)
						+ Convert.ToInt32(wyniki.W127);
				}
				{
					rest_energies += Convert.ToInt32(wyniki.W120)
						+ Convert.ToInt32(wyniki.W121)
						+ Convert.ToInt32(wyniki.W122)
						+ Convert.ToInt32(wyniki.W123)
						+ Convert.ToInt32(wyniki.W124)
						+ Convert.ToInt32(wyniki.W125)
						+ Convert.ToInt32(wyniki.W126)
						+ Convert.ToInt32(wyniki.W127);
				}
			}
			return wynik;
		}

		public static string Imie_YEC(string a, string b, char sex1, char sex2, string data1, string data2, ref int rest_energies, ref int spiritual_energies, ref int sexual_energies, ref int partnership_energies)
		{
			decimal num;
			int num1;
			a = a.ToLower();
			b = b.ToLower();
			a = a.Replace(" ", "");
			b = b.Replace(" ", "");
			a = a.Replace("ä", "ae");
			a = a.Replace("ö", "oe");
			a = a.Replace("ü", "ue");
			a = a.Replace("ß", "ss");
			b = b.Replace("ä", "ae");
			b = b.Replace("ö", "oe");
			b = b.Replace("ü", "ue");
			b = b.Replace("ß", "ss");

			data1 = data1.Replace("-", "/");
			data2 = data2.Replace("-", "/");

			sex1 = sex1 == 'K' ? 'F' : sex1;
			sex2 = sex2 == 'K' ? 'F' : sex2;

			string wynik = "";
			ylcdatabaseDataContext db = new ylcdatabaseDataContext();
			generowanieWynikuINexcelWebYECResult wyniki = null;
			generowanieWynikuINexcelWebYECResult wyniki2 = null;
			if (sex1 == 'F' && sex2 == 'F')
			{
				string _tmp = "";
				string _tmp1 = "";
				char? nullable69 = sex1;
				char? nullable70 = sex2;
				num = new decimal();
				generowanieWynikuINexcelWebYECResult imie = db.generowanieWynikuINexcelWebYEC(nullable69, nullable70, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebYECResult>();
				_tmp = string.Concat(_tmp, imie.suma, ";");
				_tmp = string.Concat(_tmp, imie.k24_1, ";");
				_tmp = string.Concat(_tmp, imie.k25_1, ";");
				_tmp = string.Concat(_tmp, imie.k31, ";");
				_tmp = string.Concat(_tmp, imie.k32, ";");
				_tmp = string.Concat(_tmp, imie.k33, ";");
				_tmp = string.Concat(_tmp, imie.k39, ";");
				_tmp = string.Concat(_tmp, imie.k42, ";");
				_tmp = string.Concat(_tmp, imie.k43, ";");
				_tmp = string.Concat(_tmp, imie.k83_a, ";");
				_tmp = string.Concat(_tmp, imie.k83_b, ";");
				_tmp = string.Concat(_tmp, imie.k83_c, ";");
				_tmp = string.Concat(_tmp, imie.k83_d, ";");
				_tmp = string.Concat(_tmp, imie.k83_e, ";");
				_tmp = string.Concat(_tmp, imie.k83_f, ";");
				_tmp = string.Concat(_tmp, imie.k88, ";");
				_tmp = string.Concat(_tmp, imie.k120, ";");
				_tmp = string.Concat(_tmp, imie.k121, ";");
				_tmp = string.Concat(_tmp, imie.k122, ";");
				_tmp = string.Concat(_tmp, imie.k123, ";");
				_tmp = string.Concat(_tmp, imie.k124, ";");
				_tmp = string.Concat(_tmp, imie.k125, ";");
				_tmp = string.Concat(_tmp, imie.k126, ";");
				_tmp = string.Concat(_tmp, imie.k127);
				wyniki = (wyniki == null ? imie : wyniki);
				char? nullable71 = sex2;
				char? nullable72 = sex1;
				num = new decimal();
				generowanieWynikuINexcelWebYECResult imie1 = db.generowanieWynikuINexcelWebYEC(nullable71, nullable72, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebYECResult>();
				_tmp1 = string.Concat(_tmp1, imie1.suma, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k24_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k25_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k31, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k32, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k33, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k39, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k42, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k43, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_a, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_b, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_c, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_d, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_e, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_f, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k88, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k120, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k121, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k122, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k123, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k124, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k125, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k126, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k127);
				wyniki2 = (wyniki2 == null ? imie1 : wyniki2);
				string[] tmp = _tmp.Split(new char[] { ';' });
				string[] tmp1 = _tmp1.Split(new char[] { ';' });
				num1 = (Convert.ToInt32(tmp[0]) + Convert.ToInt32(tmp1[0])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[1]) + Convert.ToInt32(tmp1[1])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[2]) + Convert.ToInt32(tmp1[2])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[3]) + Convert.ToInt32(tmp1[3])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[4]) + Convert.ToInt32(tmp1[4])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[5]) + Convert.ToInt32(tmp1[5])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[6]) + Convert.ToInt32(tmp1[6])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[7]) + Convert.ToInt32(tmp1[7])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[8]) + Convert.ToInt32(tmp1[8])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[9]) + Convert.ToInt32(tmp1[9])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[10]) + Convert.ToInt32(tmp1[10])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[11]) + Convert.ToInt32(tmp1[11])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[12]) + Convert.ToInt32(tmp1[12])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[13]) + Convert.ToInt32(tmp1[13])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[14]) + Convert.ToInt32(tmp1[14])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[15]) + Convert.ToInt32(tmp1[15])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[16]) + Convert.ToInt32(tmp1[16])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[17]) + Convert.ToInt32(tmp1[17])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[18]) + Convert.ToInt32(tmp1[18])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[19]) + Convert.ToInt32(tmp1[19])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[20]) + Convert.ToInt32(tmp1[20])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[21]) + Convert.ToInt32(tmp1[21])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[22]) + Convert.ToInt32(tmp1[22])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[23]) + Convert.ToInt32(tmp1[23])) / 2;
				wynik = string.Concat(wynik, num1.ToString());
			}
			else if (sex1 == 'M' && sex2 == 'M')
			{
				string _tmp = "";
				string _tmp1 = "";
				char? nullable73 = sex1;
				char? nullable74 = sex2;
				num = new decimal();
				generowanieWynikuINexcelWebYECResult imie = db.generowanieWynikuINexcelWebYEC(nullable73, nullable74, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebYECResult>();
				_tmp = string.Concat(_tmp, imie.suma, ";");
				_tmp = string.Concat(_tmp, imie.k24_1, ";");
				_tmp = string.Concat(_tmp, imie.k25_1, ";");
				_tmp = string.Concat(_tmp, imie.k31, ";");
				_tmp = string.Concat(_tmp, imie.k32, ";");
				_tmp = string.Concat(_tmp, imie.k33, ";");
				_tmp = string.Concat(_tmp, imie.k39, ";");
				_tmp = string.Concat(_tmp, imie.k42, ";");
				_tmp = string.Concat(_tmp, imie.k43, ";");
				_tmp = string.Concat(_tmp, imie.k83_a, ";");
				_tmp = string.Concat(_tmp, imie.k83_b, ";");
				_tmp = string.Concat(_tmp, imie.k83_c, ";");
				_tmp = string.Concat(_tmp, imie.k83_d, ";");
				_tmp = string.Concat(_tmp, imie.k83_e, ";");
				_tmp = string.Concat(_tmp, imie.k83_f, ";");
				_tmp = string.Concat(_tmp, imie.k88, ";");
				_tmp = string.Concat(_tmp, imie.k120, ";");
				_tmp = string.Concat(_tmp, imie.k121, ";");
				_tmp = string.Concat(_tmp, imie.k122, ";");
				_tmp = string.Concat(_tmp, imie.k123, ";");
				_tmp = string.Concat(_tmp, imie.k124, ";");
				_tmp = string.Concat(_tmp, imie.k125, ";");
				_tmp = string.Concat(_tmp, imie.k126, ";");
				_tmp = string.Concat(_tmp, imie.k127);
				wyniki = (wyniki == null ? imie : wyniki);
				char? nullable75 = sex2;
				char? nullable76 = sex1;
				num = new decimal();
				generowanieWynikuINexcelWebYECResult imie1 = db.generowanieWynikuINexcelWebYEC(nullable75, nullable76, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebYECResult>();
				_tmp1 = string.Concat(_tmp1, imie1.suma, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k24_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k25_1, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k31, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k32, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k33, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k39, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k42, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k43, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_a, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_b, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_c, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_d, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_e, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k83_f, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k88, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k120, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k121, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k122, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k123, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k124, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k125, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k126, ";");
				_tmp1 = string.Concat(_tmp1, imie1.k127);
				wyniki2 = (wyniki2 == null ? imie1 : wyniki2);
				string[] tmp = _tmp.Split(new char[] { ';' });
				string[] tmp1 = _tmp1.Split(new char[] { ';' });
				num1 = (Convert.ToInt32(tmp[0]) + Convert.ToInt32(tmp1[0])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[1]) + Convert.ToInt32(tmp1[1])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[2]) + Convert.ToInt32(tmp1[2])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[3]) + Convert.ToInt32(tmp1[3])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[4]) + Convert.ToInt32(tmp1[4])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[5]) + Convert.ToInt32(tmp1[5])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[6]) + Convert.ToInt32(tmp1[6])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[7]) + Convert.ToInt32(tmp1[7])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[8]) + Convert.ToInt32(tmp1[8])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[9]) + Convert.ToInt32(tmp1[9])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[10]) + Convert.ToInt32(tmp1[10])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[11]) + Convert.ToInt32(tmp1[11])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[12]) + Convert.ToInt32(tmp1[12])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[13]) + Convert.ToInt32(tmp1[13])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[14]) + Convert.ToInt32(tmp1[14])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[15]) + Convert.ToInt32(tmp1[15])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[16]) + Convert.ToInt32(tmp1[16])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[17]) + Convert.ToInt32(tmp1[17])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[18]) + Convert.ToInt32(tmp1[18])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[19]) + Convert.ToInt32(tmp1[19])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[20]) + Convert.ToInt32(tmp1[20])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[21]) + Convert.ToInt32(tmp1[21])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[22]) + Convert.ToInt32(tmp1[22])) / 2;
				wynik = string.Concat(wynik, num1.ToString(), ";");
				num1 = (Convert.ToInt32(tmp[23]) + Convert.ToInt32(tmp1[23])) / 2;
				wynik = string.Concat(wynik, num1.ToString());
			}
			else if (sex1 != 'M')
			{
				char? nullable77 = sex2;
				char? nullable78 = sex1;
				num = new decimal();
				generowanieWynikuINexcelWebYECResult imie = db.generowanieWynikuINexcelWebYEC(nullable77, nullable78, data2, data1, b, a, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebYECResult>();
				wynik = string.Concat(wynik, imie.suma, ";");
				wynik = string.Concat(wynik, imie.k24_1, ";");
				wynik = string.Concat(wynik, imie.k25_1, ";");
				wynik = string.Concat(wynik, imie.k31, ";");
				wynik = string.Concat(wynik, imie.k32, ";");
				wynik = string.Concat(wynik, imie.k33, ";");
				wynik = string.Concat(wynik, imie.k39, ";");
				wynik = string.Concat(wynik, imie.k42, ";");
				wynik = string.Concat(wynik, imie.k43, ";");
				wynik = string.Concat(wynik, imie.k83_a, ";");
				wynik = string.Concat(wynik, imie.k83_b, ";");
				wynik = string.Concat(wynik, imie.k83_c, ";");
				wynik = string.Concat(wynik, imie.k83_d, ";");
				wynik = string.Concat(wynik, imie.k83_e, ";");
				wynik = string.Concat(wynik, imie.k83_f, ";");
				wynik = string.Concat(wynik, imie.k88, ";");
				wynik = string.Concat(wynik, imie.k120, ";");
				wynik = string.Concat(wynik, imie.k121, ";");
				wynik = string.Concat(wynik, imie.k122, ";");
				wynik = string.Concat(wynik, imie.k123, ";");
				wynik = string.Concat(wynik, imie.k124, ";");
				wynik = string.Concat(wynik, imie.k125, ";");
				wynik = string.Concat(wynik, imie.k126, ";");
				wynik = string.Concat(wynik, imie.k127);
				wyniki = (wyniki == null ? imie : wyniki);
			}
			else
			{
				char? nullable79 = sex1;
				char? nullable80 = sex2;
				num = new decimal();
				generowanieWynikuINexcelWebYECResult imie = db.generowanieWynikuINexcelWebYEC(nullable79, nullable80, data1, data2, a, b, new decimal?(num), new int?(0), new int?(0), new int?(0), new char?('0'), "0").First<generowanieWynikuINexcelWebYECResult>();
				wynik = string.Concat(wynik, imie.suma, ";");
				wynik = string.Concat(wynik, imie.k24_1, ";");
				wynik = string.Concat(wynik, imie.k25_1, ";");
				wynik = string.Concat(wynik, imie.k31, ";");
				wynik = string.Concat(wynik, imie.k32, ";");
				wynik = string.Concat(wynik, imie.k33, ";");
				wynik = string.Concat(wynik, imie.k39, ";");
				wynik = string.Concat(wynik, imie.k42, ";");
				wynik = string.Concat(wynik, imie.k43, ";");
				wynik = string.Concat(wynik, imie.k83_a, ";");
				wynik = string.Concat(wynik, imie.k83_b, ";");
				wynik = string.Concat(wynik, imie.k83_c, ";");
				wynik = string.Concat(wynik, imie.k83_d, ";");
				wynik = string.Concat(wynik, imie.k83_e, ";");
				wynik = string.Concat(wynik, imie.k83_f, ";");
				wynik = string.Concat(wynik, imie.k88, ";");
				wynik = string.Concat(wynik, imie.k120, ";");
				wynik = string.Concat(wynik, imie.k121, ";");
				wynik = string.Concat(wynik, imie.k122, ";");
				wynik = string.Concat(wynik, imie.k123, ";");
				wynik = string.Concat(wynik, imie.k124, ";");
				wynik = string.Concat(wynik, imie.k125, ";");
				wynik = string.Concat(wynik, imie.k126, ";");
				wynik = string.Concat(wynik, imie.k127);
				wyniki = (wyniki == null ? imie : wyniki);
			}
			if (wyniki2 != null)
			{
				rest_energies += Convert.ToInt32((wyniki.W25_1 + wyniki2.W25_1) / 2);
				rest_energies += Convert.ToInt32((wyniki.W31 + wyniki2.W31) / 2);
				rest_energies += Convert.ToInt32((wyniki.W32 + wyniki2.W32) / 2);
				rest_energies += Convert.ToInt32((wyniki.W33 + wyniki2.W25_1) / 2);
				rest_energies += Convert.ToInt32((wyniki.W39 + wyniki2.W39) / 2);
				rest_energies += Convert.ToInt32((wyniki.W42 + wyniki2.W42) / 2);
				rest_energies += Convert.ToInt32((wyniki.W43 + wyniki2.W43) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_a + wyniki2.W83_a) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_b + wyniki2.W83_b) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_c + wyniki2.W83_c) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_d + wyniki2.W83_d) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_e + wyniki2.W83_e) / 2);
				rest_energies += Convert.ToInt32((wyniki.W83_f + wyniki2.W83_f) / 2);
				rest_energies += Convert.ToInt32((wyniki.W88 + wyniki2.W88) / 2);

				spiritual_energies += Convert.ToInt32((wyniki.W24_1 + wyniki2.W24_1) / 2);

				partnership_energies += Convert.ToInt32((wyniki.W120 + wyniki2.W120) / 2);
				partnership_energies += Convert.ToInt32((wyniki.W121 + wyniki2.W121) / 2);
				partnership_energies += Convert.ToInt32((wyniki.W122 + wyniki2.W122) / 2);
				partnership_energies += Convert.ToInt32((wyniki.W123 + wyniki2.W123) / 2);
				partnership_energies += Convert.ToInt32((wyniki.W124 + wyniki2.W124) / 2);
				partnership_energies += Convert.ToInt32((wyniki.W125 + wyniki2.W125) / 2);
				partnership_energies += Convert.ToInt32((wyniki.W126 + wyniki2.W126) / 2);
				partnership_energies += Convert.ToInt32((wyniki.W127 + wyniki2.W127) / 2);

			}
			else
			{
				rest_energies += Convert.ToInt32(wyniki.W25_1)
				+ Convert.ToInt32(wyniki.W31)
				+ Convert.ToInt32(wyniki.W32)
				+ Convert.ToInt32(wyniki.W33)
				+ Convert.ToInt32(wyniki.W39)
				+ Convert.ToInt32(wyniki.W42)
				+ Convert.ToInt32(wyniki.W43)
				+ Convert.ToInt32(wyniki.W83_a)
				+ Convert.ToInt32(wyniki.W83_b)
				+ Convert.ToInt32(wyniki.W83_c)
				+ Convert.ToInt32(wyniki.W83_d)
				+ Convert.ToInt32(wyniki.W83_e)
				+ Convert.ToInt32(wyniki.W83_f)
				+ Convert.ToInt32(wyniki.W88);

				spiritual_energies += Convert.ToInt32(wyniki.W24_1);

				partnership_energies += Convert.ToInt32(wyniki.W120)
					+ Convert.ToInt32(wyniki.W121)
					+ Convert.ToInt32(wyniki.W122)
					+ Convert.ToInt32(wyniki.W123)
					+ Convert.ToInt32(wyniki.W124)
					+ Convert.ToInt32(wyniki.W125)
					+ Convert.ToInt32(wyniki.W126)
					+ Convert.ToInt32(wyniki.W127);
			}
			return wynik;
		}
	}
}
