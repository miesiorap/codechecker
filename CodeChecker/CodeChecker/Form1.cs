﻿using CodeChecker.common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Windows.Forms;

namespace CodeChecker
{
    public partial class Form1 : Form
    {
        //private string format = "MM-dd-yyyy";
        private string format2 = "MM/dd/yyyy";
        Thread proces2, proces3, proces4, proces5, proces6, proces7, proces8, proces9, proces10, proces11;
        int liczbaSprawdzen = 5;
        int dzielnik4Biorytmow = 10; //5 - nowe ustalenia

        public Form1()
        {
            InitializeComponent();
            this.TopMost = true;
            this.proces2 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces3 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces4 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces5 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces6 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces7 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces8 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces9 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces10 = new Thread(new ThreadStart(SprawdzeniaWE));
            this.proces11 = new Thread(new ThreadStart(SprawdzeniaWEdobazy));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

            var proces = new Thread(new ThreadStart(SprawdzeniaWE));
            proces.Start();
            proces11.Start();
        }

        private void SprawdzeniaWE()
        {
            while (true)
            {
                ylcdatabaseDataContext we1 = new ylcdatabaseDataContext();

                #region Wysyłka maili
                var sprawdzeniaWE = we1.SprawdzeniaWE.ToList();
                if (sprawdzeniaWE.Where(swe => !string.IsNullOrWhiteSpace(swe.email) && !swe.wyslane).Any())
                {
                    string email2 = String.Empty;
                    string body = string.Empty;
                    try
                    {
                        var doWyslania = sprawdzeniaWE.Where(swe => !string.IsNullOrWhiteSpace(swe.email) && !swe.wyslane).ToList();
                        foreach (var dw in doWyslania)
                        {
                            string email = dw.email;
                            email2 = email;
                            using (StreamReader reader = new StreamReader("C://raporty/code.html"))
                            {
                                body = reader.ReadToEnd();
                            }
                            string jezyk = string.IsNullOrWhiteSpace(dw.jezyk) ? "en" : dw.jezyk;
                            if (jezyk.TrimEnd() == "de")
                            {
                                body = body.Replace("#URL#", string.Format("https://winner.energy/de/resultat/?spr={0}&data={1}&k={2}", dw.idsprawdzenia, dw.data_rozgrywki.Value.ToString("dd-MM-yyyy"), dw.kategoria));
                            }
                            else
                            {
                                body = body.Replace("#URL#", string.Format("https://winner.energy/result/?spr={0}&data={1}&k={2}", dw.idsprawdzenia, dw.data_rozgrywki.Value.ToString("dd-MM-yyyy"), dw.kategoria));
                            }
                            SendMail(email, "Your result - Winner Energy", body, null);


                            dw.wyslane = true;
                            we1.SubmitChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }

                var transferyWE = we1.TansferyWE.ToList();
                if (transferyWE.Where(twe => twe.wyslano && !string.IsNullOrWhiteSpace(twe.email)).Any())
                {
                    try
                    {
                        var doWyslania = transferyWE.Where(swe => !string.IsNullOrWhiteSpace(swe.email) && !swe.wyslano).ToList();
                        foreach (var dw in doWyslania)
                        {
                            string body = string.Empty;
                            string email = dw.email;
                            string email2 = email;
                            using (StreamReader reader = new StreamReader("C://raporty/transfer.html"))
                            {
                                body = reader.ReadToEnd();
                            }
                            string jezyk = string.IsNullOrWhiteSpace(dw.jezyk) ? "en" : dw.jezyk;
                            if (jezyk.TrimEnd() == "de")
                            {
                                body = body.Replace("#URL#", string.Format("https://winner.energy/de/transferresultat/?spr={0}&k={1}", dw.id_transferu, dw.kategoria));
                                SendMail(email, "Dein Ergebnis - Winner Energy", body, null);
                            }
                            else
                            {
                                body = body.Replace("#URL#", string.Format("https://winner.energy/transferresult/?spr={0}&k={1}", dw.id_transferu, dw.kategoria));
                                SendMail(email, "Your result - Winner Energy", body, null);
                            }


                            dw.wyslano = true;
                            we1.SubmitChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }

                #endregion

                var kolejkaSprawdzenWE = we1.KolejkaSprawdzenWE.ToList();
                string proces = Thread.CurrentThread.ManagedThreadId.ToString();
                //sprawdzenia
                if (kolejkaSprawdzenWE.Where(k => k.sprawdzono == false).Any())
                {
                    bool proceduraUdana = false;

                    try
                    {
                        RozpocznijProcesy(kolejkaSprawdzenWE);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("Błąd rozpoczęcia procesów: " + e.Message);
                    }

                    try
                    {
                        List<generowanieWynikowWEResult> lista = we1.generowanieWynikowWE(kolejkaSprawdzenWE.Where(k => k.sprawdzono == false).OrderBy(x => Guid.NewGuid()).First().idsprawdzenia).ToList();

                        foreach (var l in lista)
                        {
                            KolejkaSprawdzenWE sprawdzenie = we1.KolejkaSprawdzenWE.Where(k => k.idkolejki == l.idkolejki).First();

                            sprawdzenie.wynik = l.suma;
                            sprawdzenie.harmonia = 0;
                            sprawdzenie.duchowosc = 0;
                            sprawdzenie.seksualnosc = 0;
                            sprawdzenie.rozwoj = 0;
                            sprawdzenie.dataWynik = l.suma;
                            sprawdzenie.imieWynik = 0;
                            sprawdzenie.harmoniaWynik = 0;
                            sprawdzenie.duchowoscWynik = 0;
                            sprawdzenie.seksualnoscWynik = 0;
                            sprawdzenie.rozwojWynik = 0;
                            sprawdzenie.sprawdzono = true;
                            we1.SubmitChanges();
                        }
                        proceduraUdana = true;
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                        proceduraUdana = false;
                    }

                    //if (!proceduraUdana)
                    //{
                    //    try
                    //    {
                    //        List<KolejkaSprawdzenWE> listaSprawdzen = we1.KolejkaSprawdzenWE.Where(k => k.sprawdzono == false).OrderBy(x => Guid.NewGuid()).Take(liczbaSprawdzen).ToList();
                    //        foreach (var sprawdzenie in listaSprawdzen)
                    //        {
                    //            int rest_energies = 0;
                    //            int spiritual_energies = 0;
                    //            int sexual_energy = 0;
                    //            int partnership_energy = 0;

                    //            string d1 = sprawdzenie.date1.ToString();
                    //            string d2 = sprawdzenie.date2.ToString();
                    //            char s1 = sprawdzenie.sex1[0];
                    //            char s2 = sprawdzenie.sex2[0];
                    //            string o1 = sprawdzenie.name1;
                    //            string o2 = sprawdzenie.name2;
                    //            DateTime womanDate = new DateTime();
                    //            DateTime menDate = new DateTime();
                    //            int iPoints = 0;
                    //            int dPoints = 0;
                    //            int option = 0;
                    //            bool success_forecast = false;
                    //            if (DateTime.TryParse(d1, out womanDate) && DateTime.TryParse(d2, out menDate))
                    //            {
                    //                string data = DateCalculator.DATA_WE(womanDate.ToString(this.format2), menDate.ToString(this.format2), s1, s2, o1, o2, ref rest_energies, ref spiritual_energies, ref sexual_energy, ref partnership_energy, success_forecast);
                    //                string[] dziel = data.Split(new char[] { ';' });
                    //                dPoints = Convert.ToInt32(dziel[0]);
                    //                if (success_forecast)
                    //                {
                    //                    option = 2;
                    //                }
                    //            }
                    //            int totalPoints = dPoints + iPoints;
                    //            int percentHarmony = common.Help.GetPercentOfEnergyWE(rest_energies + sexual_energy + spiritual_energies, option);
                    //            int percentSpiritual = common.Help.GetPercentOfSpiritualEnergies(spiritual_energies, option);
                    //            int percentSexuality = common.Help.GetPercentOfSexualEnergies(sexual_energy, option);
                    //            int percentPartnership = success_forecast ? common.Help.GetPercentOfPartnershipEnergies(partnership_energy, option) : 0;

                    //            sprawdzenie.wynik = totalPoints;
                    //            sprawdzenie.harmonia = percentHarmony;
                    //            sprawdzenie.duchowosc = percentSpiritual;
                    //            sprawdzenie.seksualnosc = percentSexuality;
                    //            sprawdzenie.rozwoj = percentPartnership;
                    //            sprawdzenie.dataWynik = dPoints;
                    //            sprawdzenie.imieWynik = iPoints;
                    //            sprawdzenie.harmoniaWynik = rest_energies;
                    //            sprawdzenie.duchowoscWynik = spiritual_energies;
                    //            sprawdzenie.seksualnoscWynik = sexual_energy;
                    //            sprawdzenie.rozwojWynik = partnership_energy;
                    //            sprawdzenie.sprawdzono = true;
                    //            sprawdzenie.proces = proces;
                    //        }
                    //        we1.SubmitChanges();
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        Debug.WriteLine(e.Message);
                    //    }
                    //}
                    //else if (kolejkaSprawdzenWE.Where(k => (k.harmoniaWynik == 0 || k.harmoniaWynik == null) && k.sprawdzono).Any())
                    //{
                    //    try
                    //    {
                    //        List<KolejkaSprawdzenWE> listaSprawdzen = we1.KolejkaSprawdzenWE.Where(k => (k.harmoniaWynik == 0 || k.harmoniaWynik == null) && k.sprawdzono).OrderBy(x => Guid.NewGuid()).Take(liczbaSprawdzen).ToList();
                    //        foreach (var sprawdzenie in listaSprawdzen)
                    //        {
                    //            int rest_energies = 0;
                    //            int spiritual_energies = 0;
                    //            int sexual_energy = 0;
                    //            int partnership_energy = 0;

                    //            string d1 = sprawdzenie.date1.ToString();
                    //            string d2 = sprawdzenie.date2.ToString();
                    //            char s1 = sprawdzenie.sex1[0];
                    //            char s2 = sprawdzenie.sex2[0];
                    //            string o1 = sprawdzenie.name1;
                    //            string o2 = sprawdzenie.name2;
                    //            DateTime womanDate = new DateTime();
                    //            DateTime menDate = new DateTime();
                    //            int iPoints = 0;
                    //            int dPoints = 0;
                    //            int option = 0;
                    //            bool success_forecast = false;
                    //            if (DateTime.TryParse(d1, out womanDate) && DateTime.TryParse(d2, out menDate))
                    //            {
                    //                string data = DateCalculator.DATA_WE(womanDate.ToString(this.format2), menDate.ToString(this.format2), s1, s2, o1, o2, ref rest_energies, ref spiritual_energies, ref sexual_energy, ref partnership_energy, success_forecast);
                    //                string[] dziel = data.Split(new char[] { ';' });
                    //                dPoints = Convert.ToInt32(dziel[0]);
                    //                if (success_forecast)
                    //                {
                    //                    option = 2;
                    //                }
                    //            }
                    //            int totalPoints = dPoints + iPoints;
                    //            int percentHarmony = common.Help.GetPercentOfEnergyWE(rest_energies + sexual_energy + spiritual_energies, option);
                    //            int percentSpiritual = common.Help.GetPercentOfSpiritualEnergies(spiritual_energies, option);
                    //            int percentSexuality = common.Help.GetPercentOfSexualEnergies(sexual_energy, option);
                    //            int percentPartnership = success_forecast ? common.Help.GetPercentOfPartnershipEnergies(partnership_energy, option) : 0;

                    //            sprawdzenie.wynik = totalPoints;
                    //            sprawdzenie.harmonia = percentHarmony;
                    //            sprawdzenie.duchowosc = percentSpiritual;
                    //            sprawdzenie.seksualnosc = percentSexuality;
                    //            sprawdzenie.rozwoj = percentPartnership;
                    //            sprawdzenie.dataWynik = dPoints;
                    //            sprawdzenie.imieWynik = iPoints;
                    //            sprawdzenie.harmoniaWynik = rest_energies;
                    //            sprawdzenie.duchowoscWynik = spiritual_energies;
                    //            sprawdzenie.seksualnoscWynik = sexual_energy;
                    //            sprawdzenie.rozwojWynik = partnership_energy;
                    //            sprawdzenie.proces = proces;
                    //        }
                    //        we1.SubmitChanges();
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        Debug.WriteLine(e.Message);
                    //    }
                    //}
                }
                else
                {
                    proces2.Abort();
                    proces3.Abort();
                    proces4.Abort();
                    proces5.Abort();
                    proces6.Abort();
                    proces7.Abort();
                    proces8.Abort();
                    proces9.Abort();
                    proces10.Abort();
                    Thread.Sleep(10000);
                }
            }
        }

        private void SprawdzeniaWEdobazy()
        {
            var skip = 0;
            while (true)
            {
                ylcdatabaseDataContext we1 = new ylcdatabaseDataContext();

                var kolejka = we1.KolejkaSprawdzenWE.OrderByDescending(we => we.idkolejki).Skip(skip).Take(15).ToList();
                skip += 15;
                var baza = we1.bazaWE.ToList();
                var sprawdzenia = we1.SprawdzeniaWE.ToList();

                List<bazaWE> rekordyDoWstawienia = new List<bazaWE>();
                for (int index = 0; index < kolejka.Count; index++)
                {
                    var k = kolejka[index];
                    if (baza.Where(b => b.idsprawdzenia == k.idkolejki).Any())
                    {
                        kolejka.RemoveAt(index);
                    }
                }

                for (int index = 0; index < kolejka.Count; index++)
                {
                    var k = kolejka[index];
                    try
                    {
                        if (baza.Where(bz => bz.data1 == k.date1 && bz.data2 == k.date2 && bz.plec1 == k.sex1[0] && bz.plec2 == k.sex2[0] && bz.osoba1 == k.name1 && bz.osoba2 == k.name2).Any())
                        {
                            continue;
                        }

                        SprawdzeniaWE sp = sprawdzenia.Where(spr => spr.idsprawdzenia == k.idsprawdzenia).First();
                        if (sp.data_rozgrywki == null)
                        {
                            continue;
                        }

                        int h = 0;
                        int s = 0;
                        int d = 0;
                        int r = 0;
                        var data = DateCalculator.DATA_WE_w(k.date1.ToString("MM-dd-yyyy"), k.date2.ToString("MM-dd-yyyy"), k.sex1[0], k.sex2[0], k.name1, k.name2, ref h, ref d, ref s, ref r, true);
                        var dS = data.Split(';');
                        int hData = h;
                        int dData = d;
                        int sData = s;
                        int rData = r;
                        var imie = NamesCalculator.Imie_w(k.name1, k.name2, k.sex1[0], k.sex2[0], k.date1.ToString("MM-dd-yyyy"), k.date2.ToString("MM-dd-yyyy"), ref h, ref d, ref s, ref r, true);
                        var iS = imie.Split(';');

                        bazaWE b = new bazaWE();
                        b.projekt = "Winner Energy";
                        b.idsprawdzenia = k.idkolejki;
                        b.data1 = k.date1;
                        b.data2 = k.date2;
                        b.plec1 = k.sex1[0];
                        b.plec2 = k.sex2[0];
                        b.osoba1 = k.name1;
                        b.osoba2 = k.name2;

                        b.HarmoniaData = hData + rData;
                        b.SeksualnoscData = sData;
                        b.DuchowoscData = dData;
                        b.VipRozwojData = rData;
                        b.VipSeksualnoscData = sData;
                        b.VipDuchowoscData = dData;
                        b.VipHarmoniaData = hData;

                        b.HarmoniaDataImie = h + r;
                        b.SeksualnoscDataImie = s;
                        b.DuchowoscDataImie = d;
                        b.VipRozwojDataImie = r;
                        b.VipSeksualnoscDataImie = s;
                        b.VipDuchowoscDataImie = d;
                        b.VipHarmoniaDataImie = h;

                        b.sumaData = Convert.ToInt32(dS[0]);
                        b._01__W_Zgodność_Nakszatr__LC1_____I = Convert.ToInt32(dS[1]);
                        b._02__Przyjaźń__LC1A_____I = Convert.ToInt32(dS[2]);
                        b._03__Neutralność__LC1B_____I = Convert.ToInt32(dS[3]);
                        b._04__Wrogość__LC1C_____I = Convert.ToInt32(dS[4]);
                        b._05__Temperament__LC1DEF_____I = Convert.ToInt32(dS[5]);
                        b._06__W_Zgodność_SEX__LC2_____I = Convert.ToInt32(dS[6]);
                        b._07__Zgodność_SEX_wierz__LC2W____I = Convert.ToInt32(dS[7]);
                        b._08__Zgodność_SEX____LC2A_____I = Convert.ToInt32(dS[8]);
                        b._09__Antagoniczność_SEX__LC2B_____I = Convert.ToInt32(dS[9]);
                        b._10__W_Zgodność_duchowa__LC3_____I = Convert.ToInt32(dS[10]);
                        b._11__Duchowe_podobieństwo____I = Convert.ToInt32(dS[11]);
                        b._12__Karmiczne__LC1H_____I = Convert.ToInt32(dS[12]);
                        b._13__Duchowo_Obciążone__LC1G_____I = Convert.ToInt32(dS[13]);
                        b._21b__Num_1__4_10___LC4A_VB_____NU = Convert.ToInt32(dS[14]);
                        b._22__W_Num_2__________LC4B_____NU = Convert.ToInt32(dS[15]);
                        b._34__Wibarcja_4__LC19D_____NU = Convert.ToInt32(dS[16]);
                        b._35__Wibarcja_5__LC19E_____NU = Convert.ToInt32(dS[17]);
                        b._36__Wibarcja_6__LC19F_____NU = Convert.ToInt32(dS[18]);
                        b._37__Wibracja_ALL__LC19_____NU = Convert.ToInt32(dS[19]);
                        b._38__W_Wspólne_cyfry___data__LC9_____NU = Convert.ToInt32(dS[20]);
                        b._41__LC22a____NU = Convert.ToInt32(dS[21]);
                        b._45__Wspólne_daty_DDMMRRR____NU = Convert.ToInt32(dS[22]);
                        b._51___LC6A_____EU = Convert.ToInt32(dS[23]);
                        b._52___LC6B_____EU = Convert.ToInt32(dS[24]);
                        b._53__Znak_EUR__Lok_zw____LC8_____EU = Convert.ToInt32(dS[25]);
                        b._54___LC10_____EU = Convert.ToInt32(dS[26]);
                        b._55__EUR1_Żywioł___element__LC17_____EU = Convert.ToInt32(dS[27]);
                        b._56__LC22c____EU = Convert.ToInt32(dS[28]);
                        b._58__W_EUR_Żywioł____LC17A_GS_____EU = Convert.ToInt32(dS[29]);
                        b._59__W_EUR_Znak____LC17B_I_GS_____EU = Convert.ToInt32(dS[30]);
                        b._60__W_EUR_Znak____LC17B_II_GS_____EU = Convert.ToInt32(dS[31]);
                        b._61__EUR_Znak_neg__LC17D_GS_____EU = Convert.ToInt32(dS[32]);
                        b._65__Biorytm_Sukcesu = Convert.ToInt32(dS[33]);
                        b._66__Biorytm_Fizyczny_wierzchołek = Convert.ToInt32(dS[34]);
                        b._67__Biorytm_Emocjonalny_wierzchołek = Convert.ToInt32(dS[35]);
                        b._68__Biorytm_Fizyczny_sex = Convert.ToInt32(dS[36]);
                        b._69__Biorytm_Emocjonalny_sex = Convert.ToInt32(dS[37]);
                        b._70a__1_Biorytm_Fizyczny___Emocjanalny = Convert.ToInt32(dS[38]);
                        b._70b__0_Biorytm_Fizyczny___Emocjanalny = Convert.ToInt32(dS[39]);
                        b._70c__1_Biorytm_Fizyczny___Emocjanalny___Intelektualny = Convert.ToInt32(dS[40]);
                        b._71__W_CH1_ZW_ŻY_JJ__LC11_____CH = Convert.ToInt32(dS[41]);
                        b._72__W_CH2_wierzchołki___6__LC12_____CH = Convert.ToInt32(dS[42]);
                        b._73__W_CH2_wierzchołki___harmoniczne__LC13_____CH = Convert.ToInt32(dS[43]);
                        b._74__CH2_wierzchołki___nie_harmo__LC14_____CH = Convert.ToInt32(dS[44]);
                        b._76__W_CH1_wierzchołki__LC18_____CH = Convert.ToInt32(dS[45]);
                        b._77__CH1_Zwierze__LC11A_____CH = Convert.ToInt32(dS[46]);
                        b._78__CH1_Żywioł__LC11B_____CH = Convert.ToInt32(dS[47]);
                        b._79a__CH1_JANG__LC11C1_____CH = Convert.ToInt32(dS[48]);
                        b._79b__CH1_JIN__LC11C2_____CH = Convert.ToInt32(dS[49]);
                        b._79c__CH1_JIN_JANG__LC11C3_____CH = Convert.ToInt32(dS[50]);
                        b._81a__Eneagram_SERCE_D____ENE = Convert.ToInt32(dS[51]);
                        b._81b__Eneagram_GŁOWA_D____ENE = Convert.ToInt32(dS[52]);
                        b._81c__Eneagram_BRZUCH_D____ENE = Convert.ToInt32(dS[53]);
                        b._81d__Eneagram_SERCE_GLOWA_D____ENE = Convert.ToInt32(dS[54]);
                        b._81e__Eneagram_SERCE_BRZUCH_D____ENE = Convert.ToInt32(dS[55]);
                        b._81f__Eneagram_GLOWA_BRZUCH_D____ENE = Convert.ToInt32(dS[56]);
                        b._87__Eneagram_D__LC16b_____ENE = Convert.ToInt32(dS[57]);
                        b._118__Suma_Wektor_Data = Convert.ToInt32(dS[58]);
                        b._110__Wektor_W123_Data = Convert.ToInt32(dS[59]);
                        b._111__Wektor_W456_Data = Convert.ToInt32(dS[60]);
                        b._112__Wektor_W789_Data = Convert.ToInt32(dS[61]);
                        b._113__Wektor_W147_Data = Convert.ToInt32(dS[62]);
                        b._114__Wektor_W258_Data = Convert.ToInt32(dS[63]);
                        b._115__Wektor_W369_Data = Convert.ToInt32(dS[64]);
                        b._116__Wektor_W159_Data = Convert.ToInt32(dS[65]);
                        b._117__Wektor_W357_Data = Convert.ToInt32(dS[66]);
                        b._129__Małżeństwa_LC25 = Convert.ToInt32(dS[67]);
                        b._130__Tara_Bala_LC26 = Convert.ToInt32(dS[68]);
                        b._131__Tara_Bala_LC26 = Convert.ToInt32(dS[69]);
                        b._132__Tara_Bala_LC26 = Convert.ToInt32(dS[70]);

                        b.sumaIN = Convert.ToInt32(iS[0]);
                        b.SUMA_D_IN = b.sumaData + b.sumaIN;
                        b._24_1__Num_1__4_10___I_N__LC5A1psn_____NU = Convert.ToInt32(iS[1]);
                        b._25_1__Num_2__________I_N__LC5A2psn_____NU = Convert.ToInt32(iS[2]);
                        b._31__Wibarcja_1__LC19A_____NU = Convert.ToInt32(iS[3]);
                        b._32__Wibarcja_2__LC19B_____NU = Convert.ToInt32(iS[4]);
                        b._33__Wibarcja_3__LC19C_____NU = Convert.ToInt32(iS[5]);
                        b._39__W_Wspólne_cyfry_I_N_PSN____NU = Convert.ToInt32(iS[6]);
                        b._42__LC22b____NU = Convert.ToInt32(iS[7]);
                        b._43__Wspólne_znaki_PSN____NU = Convert.ToInt32(iS[8]);
                        b._83a__Eneagram_SERCE_I_N____ENE = Convert.ToInt32(iS[9]);
                        b._83b__Eneagram_GŁOWA_I_N____ENE = Convert.ToInt32(iS[10]);
                        b._83c__Eneagram_BRZUCH_I_N____ENE = Convert.ToInt32(iS[11]);
                        b._83d__Eneagram_SERCE_GLOWA_I_N____ENE = Convert.ToInt32(iS[12]);
                        b._83e__Eneagram_SERCE_BRZUCH_I_N____ENE = Convert.ToInt32(iS[13]);
                        b._83f__Eneagram_GLOWA_BRZUCH_I_N____ENE = Convert.ToInt32(iS[14]);
                        b._88__Eneagram_I_N__LC16b_____ENE = Convert.ToInt32(iS[15]);
                        b._120__Wektor_W123_INpsn = Convert.ToInt32(iS[16]);
                        b._121__Wektor_W456_INpsn = Convert.ToInt32(iS[17]);
                        b._122__Wektor_W789_INpsn = Convert.ToInt32(iS[18]);
                        b._123__Wektor_W147_INpsn = Convert.ToInt32(iS[19]);
                        b._124__Wektor_W258_INpsn = Convert.ToInt32(iS[20]);
                        b._125__Wektor_W369_INpsn = Convert.ToInt32(iS[21]);
                        b._126__Wektor_W159_INpsn = Convert.ToInt32(iS[22]);
                        b._127__Wektor_W357__INpsn_ = Convert.ToInt32(iS[23]);

                        b.Procent = common.Help.GetPercentOfEnergyWE((int)b.sumaData, 0).ToString();
                        b.Gwiazdki = common.Help.GetGwiazdki((int)b.sumaData);

                        try
                        {
                            b.DataBiorytmu = sp.data_rozgrywki;
                            double f = common.Help.ObliczBiorytmFizyczny2(b.data1.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));
                            double i = common.Help.ObliczBiorytmIntelektualny2(b.data1.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));
                            double e = common.Help.ObliczBiorytmEmocjonalny2(b.data1.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));

                            double d2 = common.Help.ObliczBiorytmDuchowy(b.data1.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));
                            double i2 = common.Help.ObliczBiorytmIntuicyjny(b.data1.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));

                            double s2 = common.Help.ObliczBiorytmSwiadomosci(b.data1.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));
                            double es = common.Help.ObliczBiorytmEstetyczny(b.data1.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));

                            double fw, iw, ew, dw, i2w, sw, esw = 0;
                            fw = f * 100;
                            iw = i * 100;
                            ew = e * 100;
                            dw = d2 * 100;
                            i2w = i2 * 100;
                            sw = s2 * 100;
                            esw = es * 100;

                            b.Biorytm_fizyczny1 = fw.ToString();
                            b.Biorytm_intelektualny1 = iw.ToString();
                            b.Biorytm_emocjonalny1 = ew.ToString();
                            b.Biorytm_estetyczny1 = esw.ToString();
                            b.Biorytm_intuicji1 = i2w.ToString();
                            b.Biorytm_świadomości1 = sw.ToString();
                            b.Biorytm_duchowy1 = dw.ToString();

                            //wyliczanie procentu od -100 do 100
                            f = (1 + f) / 2;
                            e = (1 + e) / 2;
                            i = (1 + i) / 2;
                            d2 = (1 + d) / 2;
                            i2 = (1 + i2) / 2;
                            s2 = (1 + s) / 2;
                            es = (1 + es) / 2;

                            //liczba procentowa
                            f *= 100;
                            e *= 100;
                            i *= 100;
                            d2 *= 100;
                            i2 *= 100;
                            s2 *= 100;
                            es *= 100;

                            b.Procent_biorytm1 = common.Help.CeilingOrFloor((f + e + i) / 3).ToString();
                            b.Procent_biorytm1_7_biorytmow = common.Help.CeilingOrFloor(common.Help.CeilingOrFloor((f + e + i) / 3) + (common.Help.CeilingOrFloor((d2 + +i2 + s2 + es) / 4) / dzielnik4Biorytmow)).ToString();

                            f = common.Help.ObliczBiorytmFizyczny2(b.data2.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));
                            i = common.Help.ObliczBiorytmIntelektualny2(b.data2.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));
                            e = common.Help.ObliczBiorytmEmocjonalny2(b.data2.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));

                            d2 = common.Help.ObliczBiorytmDuchowy(b.data2.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));
                            i2 = common.Help.ObliczBiorytmIntuicyjny(b.data2.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));

                            s2 = common.Help.ObliczBiorytmSwiadomosci(b.data2.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));
                            es = common.Help.ObliczBiorytmEstetyczny(b.data2.Value.ToString("dd-MM-yyyy"), b.DataBiorytmu.Value.ToString("dd-MM-yyyy"));

                            fw = f * 100;
                            iw = i * 100;
                            ew = e * 100;
                            dw = d2 * 100;
                            i2w = i2 * 100;
                            sw = s2 * 100;
                            esw = es * 100;

                            b.Biorytm_fizyczny2 = fw.ToString();
                            b.Biorytm_intelektualny2 = iw.ToString();
                            b.Biorytm_emocjonalny2 = ew.ToString();
                            b.Biorytm_estetyczny2 = esw.ToString();
                            b.Biorytm_intuicji2 = i2w.ToString();
                            b.Biorytm_świadomości2 = sw.ToString();
                            b.Biorytm_duchowy2 = dw.ToString();

                            //wyliczanie procentu od -100 do 100
                            f = (1 + f) / 2;
                            e = (1 + e) / 2;
                            i = (1 + i) / 2;
                            d2 = (1 + d2) / 2;
                            i2 = (1 + i2) / 2;
                            s2 = (1 + s2) / 2;
                            es = (1 + es) / 2;

                            //liczba procentowa
                            f *= 100;
                            e *= 100;
                            i *= 100;
                            d2 *= 100;
                            i2 *= 100;
                            s2 *= 100;
                            es *= 100;

                            b.Procent_biorytm2 = common.Help.CeilingOrFloor((f + e + i) / 3).ToString();
                            b.Procent_biorytm2_7_biorytmow = common.Help.CeilingOrFloor(common.Help.CeilingOrFloor((f + e + i) / 3) + (common.Help.CeilingOrFloor((d2 + +i2 + s2 + es) / 4) / dzielnik4Biorytmow)).ToString();
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.Message);
                        }

                        rekordyDoWstawienia.Add(b);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                }
                if (rekordyDoWstawienia.Count > 0)
                {
                    we1.bazaWE.InsertAllOnSubmit(rekordyDoWstawienia);
                    we1.SubmitChanges();
                }
                we1.Dispose();
                Thread.Sleep(10000);
            }
        }

        private void SendMail(string email, string subject, string content, string attachment = null)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("info@winner.energy");
                mailMessage.Subject = subject;
                mailMessage.Body = content;
                mailMessage.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(attachment))
                {
                    mailMessage.Attachments.Add(new Attachment(attachment));
                }
                mailMessage.To.Add(new MailAddress(email));
                SmtpClient smtp = new SmtpClient()
                {
                    Host = "smtp.ionos.de",
                    EnableSsl = true
                };
                NetworkCredential NetworkCred = new NetworkCredential()
                {
                    UserName = "info@winner.energy",
                    Password = "Prodigy3439!"
                };
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mailMessage);
            }
        }

        private void SendMail2(string email, string subject, string content, string attachment = null)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("info2@winner.energy");
                mailMessage.Subject = subject;
                mailMessage.Body = content;
                mailMessage.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(attachment))
                {
                    mailMessage.Attachments.Add(new Attachment(attachment));
                }
                mailMessage.To.Add(new MailAddress(email));
                SmtpClient smtp = new SmtpClient()
                {
                    Host = "smtp.ionos.de",
                    EnableSsl = true
                };
                NetworkCredential NetworkCred = new NetworkCredential()
                {
                    UserName = "info2@winner.energy",
                    Password = "Prodigy3439!"
                };
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mailMessage);
            }
        }

        private void RozpocznijProcesy(List<KolejkaSprawdzenWE> kolejkaSprawdzenWE)
        {
            proces11.Start();
            try
            {
                proces2.Start();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Proces 2: " + e.Message);
            }
            try
            {
                proces3.Start();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Proces 3: " + e.Message);
            }
            try
            {
                proces4.Start();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Proces 4: " + e.Message);
            }
            try
            {
                proces5.Start();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Proces 5: " + e.Message);
            }
            if (kolejkaSprawdzenWE.Where(k => k.sprawdzono == false).ToList().Count > 100)
            {
                try
                {
                    proces6.Start();
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Proces 6: " + e.Message);
                }
                try
                {
                    proces7.Start();
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Proces 7: " + e.Message);
                }
                try
                {
                    proces8.Start();
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Proces 8: " + e.Message);
                }
                try
                {
                    proces9.Start();
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Proces 9: " + e.Message);
                }
                try
                {
                    proces10.Start();
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Proces 10: " + e.Message);
                }
                try
                {
                    proces11.Start();
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Proces 11: " + e.Message);
                }
            }
        }
    }
}